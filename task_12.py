# https://www.codewars.com/kata/585d7d5adb20cf33cb000235
from collections import Counter


def find_uniq(arr):
    c = Counter(arr)
    for key, value in c.items():
        if value == 1:
            return key


print(find_uniq([1, 1, 1, 2, 1, 1]))  # 2
print(find_uniq([0, 0, 0.55, 0, 0]))  # 0.55
print(find_uniq([3, 10, 3, 3, 3]))  # 10
