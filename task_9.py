# https://www.codewars.com/kata/52c31f8e6605bcc646000082


def two_sum(numbers, target):
    return next([i, i + 1 + j]
                for i, x in enumerate(numbers)
                for j, y in enumerate(numbers[i + 1:])
                if x + y == target)


print(two_sum([1, 2, 3], 4))  # 0, 2
print(two_sum([2, 2, 3], 4))  # 0, 1
