# https://www.codewars.com/kata/55fd2d567d94ac3bc9000064
import collections


def row_sum_odd_numbers(n):
    square_n = n*n

    if square_n % 2 != 0:
        deq = collections.deque([square_n])
        nearest_left_odd_number = nearest_right_odd_number = square_n
    else:
        deq = collections.deque([square_n - 1, square_n + 1])
        nearest_left_odd_number = square_n - 1
        nearest_right_odd_number = square_n + 1
    while len(deq) < n:
        nearest_left_odd_number -= 2
        nearest_right_odd_number += 2
        deq.appendleft(nearest_left_odd_number)
        deq.append(nearest_right_odd_number)
    return sum(deq)


print(row_sum_odd_numbers(3))  # 7 + 9 + 11 = 27
print(row_sum_odd_numbers(13))  # 2197
