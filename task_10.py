# https://www.codewars.com/kata/52efefcbcdf57161d4000091
from collections import Counter


def count(string):
    if string:
        return dict(Counter(string))
    else:
        return {}


print(count('aba'))  # {'a': 2, 'b': 1}
print(count(''))  # {}
