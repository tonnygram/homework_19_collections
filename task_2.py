# https://www.codewars.com/kata/55f2b110f61eb01779000053


def get_sum(a, b):
    # return sum(x for x in range(a, b+1))
    if a < b:
        return sum(x for x in range(a, b+1))
    else:
        return sum([x for x in range(b, a + 1)])


print(get_sum(-1, 2))  # -1 + 0 + 1 + 2 = 2
print(get_sum(0, -1))  # -1
