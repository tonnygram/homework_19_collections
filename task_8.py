# https://www.codewars.com/kata/57eb8fcdf670e99d9b000272


def high(x):
    highest = ''
    max_score = 0
    for word in x.split():
        score = 0
        for char in word:
            score += ord(char) - 96
        if score > max_score:
            highest = word
            max_score = score
    return highest


print(high('man i need a taxi up to ubud'))  # taxi
