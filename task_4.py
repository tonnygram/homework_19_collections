# https://www.codewars.com/kata/55b42574ff091733d900002f


def friend(x):
    return [word for word in x if len(word) == 4]


print(friend(['Ryan', 'Kieran', 'Mark']))
