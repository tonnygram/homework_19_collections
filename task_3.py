# https://www.codewars.com/kata/563cf89eb4747c5fb100001b


def remove_smallest(numbers):
    new_list = numbers.copy()
    if new_list:
        new_list.pop(new_list.index(min(new_list)))
    else:
        new_list = []
    return new_list


print(remove_smallest([1, 2, 3, 4, 5]))
