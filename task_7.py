# https://www.codewars.com/kata/578553c3a1b8d5c40300037c


def binary_array_to_number(arr):
    return int(''.join([str(x) for x in arr]), 2)


print(binary_array_to_number([0, 1, 1, 0]))  # 6
